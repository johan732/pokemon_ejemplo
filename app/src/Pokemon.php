<?php

namespace App\src;

use Illuminate\Database\Eloquent\Model;

class Pokemon extends Model
{
    protected $table = 'pokemones';
    protected $fillable = ['id', 'tipos_id', 'pokemon', 'entrenador'];

    /**
     * Un pokemon es de un solo tipo
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function Tipo()
    {
        return $this->hasOne('App\src\Tipo', 'id', 'tipos_id');
    }
}
