<?php

namespace App\Http\Controllers\pokemon;

use App\Http\Requests\PokemonRequest;
use App\src\Pokemon;
use App\src\Tipo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PokemonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pokemones = Pokemon::orderBy('pokemon', 'ASC')->paginate(10);

        return view('pokemon.pokedex')->with('pokemones', $pokemones);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tipos = Tipo::orderBy('tipo', 'ASC')->get();
        return view('pokemon.create')->with('tipos', $tipos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(PokemonRequest $request)
    {
        $pokemon = new Pokemon($request->all());
        $pokemon->save();
        flash('Pokemon registrado', 'success');
        return redirect()->route('pokedex.index');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pokemon = Pokemon::find($id);
        $tipos = Tipo::orderBy('tipo', 'ASC')->get();

        return view('pokemon.edit')
            ->with('pokemon', $pokemon)
            ->with('tipos', $tipos);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(PokemonRequest $request, $id)
    {
        $pokemon = Pokemon::find($id);
        $pokemon->fill($request->all());
        $pokemon->save();

        flash('Pokemon actualizado', 'success');
        return redirect()->route('pokedex.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pokemon = Pokemon::find($id);
        $pokemon->delete();
        flash('Pokemon eliminado', 'danger');
        return redirect()->route('pokedex.index');
    }
}
