<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Routing\Route;

class PokemonRequest extends FormRequest
{

    /**
     * @var Route
     */
    private $route;

    /**
     * PokemonRequest constructor.
     * @param Route $route
     */
    public function __construct(Route $route)
    {
        $this->route = $route;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'entrenador.min' => 'El nombre del entrenador debe tener por lo menos 2 carateres'
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tipos_id' => 'required',
            'pokemon' => 'min:2|required',
            'entrenador' => 'min:2|required'
        ];
    }
}
