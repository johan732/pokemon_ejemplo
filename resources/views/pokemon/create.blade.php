@extends('plantilla.app')
@section('title')
    Pokedex
@endsection

@section('content')
    <form method="POST" action="{{route('pokemon.store')}}">
        {{csrf_field()}}
        <div class="card">
            <div class="card-header text-white bg-primary mb-3">Nuevo pokemón</div>
            <div class="card-body">
                <div class="form-group">

                    <div class="form-group text-left">
                        <label>Tipo:</label>
                        <select class="form-control" required name="tipos_id">
                            <option>Seleccione una opción</option>
                            @if(count($tipos))
                                @foreach($tipos as $tipo)
                                    <option value="{{$tipo->id}}">{{$tipo->tipo}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>

                    <div class="form-group text-left">
                        <label>Pokemón:</label>
                        <input type="text" class="form-control" name="pokemon" placeholder="Pokemón" required>
                    </div>
                    <div class="form-group text-left">
                        <label>Entrenador:</label>
                        <input type="text" class="form-control" name="entrenador" placeholder="Entrenador" required>
                    </div>

                    <div class="text-center">
                        <input type="submit" class="btn btn-primary" value="Guardar">
                    </div>

                </div>
            </div>
        </div>
    </form>
@endsection
