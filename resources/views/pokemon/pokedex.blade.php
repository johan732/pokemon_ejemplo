@extends('plantilla.app')
@section('title')
    Pokedex
@endsection

@section('content')
    <div class="card">
        <div class="card-header text-white bg-primary mb-3">Pokedex</div>
        <div class="card-body">
            @if(count($pokemones))
                <table class="table table-bordered">
                    <thead>
                    <th>Tipo</th>
                    <th>Nombre</th>
                    <th>Entrenador</th>
                    <th>Fecha de captura</th>
                    <th>Acciones</th>
                    </thead>

                    <tbody>
                    @foreach($pokemones As $pokemon)
                        <tr>
                            <td>{{$pokemon->Tipo->tipo}}</td>
                            <td>{{$pokemon->pokemon}}</td>
                            <td>{{$pokemon->entrenador}}</td>
                            <td>{{$pokemon->created_at}}</td>
                            <td>
                                <a href="{{route('pokemon.edit',$pokemon->id)}}"
                                   class="text-white btn btn-warning">
                                    Editar
                                </a>
                                <a href="{{route('pokemon.destroy',$pokemon->id)}}"
                                   onclick=" return confirm('Desea eliminar este pokemon?')"
                                   class="text-white btn btn-danger">
                                    Eliminar
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {!! $pokemones->render() !!}
            @else
                <h3 class="text-center"> No hay pokemones registrados </h3>
            @endif
        </div>
    </div>
@endsection
