<!-- La variable $errors es propia de laravel -->

@if(!$errors->isEmpty())
    <div class="alert alert-danger">
        <p>
            <strong>Oops!</strong>
            Por favor tenga en cuenta lo siguiente:
        </p>
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>

    </div>
@endif