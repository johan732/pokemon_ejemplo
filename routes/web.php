<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pokemon.index');
});

Route::get('pokedex', [
    'uses' => 'pokemon\PokemonController@index',
    'as' => 'pokedex.index'
]);

Route::get('pokemon/create', [
    'uses' => 'pokemon\PokemonController@create',
    'as' => 'pokemon.create'
]);

Route::post('pokemon', [
    'uses' => 'pokemon\PokemonController@store',
    'as' => 'pokemon.store'
]);

Route::get('pokemon/{id}/edit', [
    'uses' => 'pokemon\PokemonController@edit',
    'as' => 'pokemon.edit'
]);

Route::put('pokemon/{id}', [
    'uses' => 'pokemon\PokemonController@update',
    'as' => 'pokemon.update'
]);

Route::get('pokemon/{id}/destroy', [
    'uses' => 'pokemon\PokemonController@destroy',
    'as' => 'pokemon.destroy'
]);

